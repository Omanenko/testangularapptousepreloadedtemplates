import { BhComponent } from './../utils/BhComponent';

@BhComponent({
  selector: 'app-foo',
  templateUrl: './foo.component.html'
})
export class FooComponent {
  foo = 'foo';
}
