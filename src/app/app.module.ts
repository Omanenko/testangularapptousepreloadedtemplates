import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BooComponent } from './boo/boo.component';
import { FooComponent } from './foo/foo.component';

@NgModule({
  declarations: [AppComponent, BooComponent, FooComponent],
  imports: [BrowserModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
