import { Component } from '@angular/core';

const findTemplate = templateUrl => {
  const elt = document.getElementById(templateUrl);
  const template =
    elt && elt.getAttribute('type') === 'text/ng-template'
      ? elt.innerHTML
      : null;
  console.log(`Found preload template for ${templateUrl}`, template !== null);
  return template;
};

export const BhComponent = componentDef => {
  const templateUrl = componentDef.templateUrl;
  return Component({
    ...componentDef,
    templateUrl: null,
    get template() {
      console.log('Read ', templateUrl);
      return findTemplate(templateUrl);
    }
  });
};
