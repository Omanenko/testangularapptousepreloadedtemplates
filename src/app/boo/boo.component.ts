import { BhComponent } from '../utils/BhComponent';

@BhComponent({
  selector: 'app-boo',
  templateUrl: './boo.component.html'
})
export class BooComponent {
  boo = 'boo';
}
