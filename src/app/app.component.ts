import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'as-main-app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'cap';
}
